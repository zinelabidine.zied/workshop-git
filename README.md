# Prise en main de git 

totototototototototo

## Dans ce dépôt, vous trouverez  

* un cours d'introduction à git (le cours est déja envoyé par le formateur)
* un tutoriel de prise en main de [git](https://git-scm.com) en ligne de commande (construit à partir des exemples du cours précédent) : [ici](git/git_premierspas.md) 


## Des liens vers des exercices interactifs pour manipuler les commandes git

* [Resources to learn git](https://try.github.io)
* [Learn Git Branching](http://learngitbranching.js.org/) : apprendre le branching sous git
* [Visualizing git](http://git-school.github.io/visualizing-git) et [Git Gud](https://nic-hartley.github.io/git-gud) : des simulateurs web pour manipuler/visualiser les commandes et mieux comprendre les workflows.
* [Git-It](https://github.com/jlord/git-it-electron) : application multi-plateforme ([à installer](https://github.com/jlord/git-it-electron/releases/latest)) proposant des défis utilisant *vraiment* git et GitHub sans passer par un émulateur...


## Des liens à garder sous la main

Le site de référence : [https://git-scm.com](https://git-scm.com) et sa [documentation]((https://git-scm.com/doc)) ainsi que le [Pro Git book (guide officiel) en version française](https://git-scm.com/book/fr/v2)  
Des tutoriels sur git bien illustrés : [Devenez un gourou du Git (atlassian)](https://fr.atlassian.com/git/tutorials/)  
Des aides-mémoires pour les commandes git : [ici](https://services.github.com/on-demand/downloads/github-git-cheat-sheet.pdf), [là](https://education.github.com/git-cheat-sheet-education.pdf) ou encore [ici](https://zeroturnaround.com/rebellabs/git-commands-and-best-practices-cheat-sheet/) et [là](https://www.git-tower.com/blog/git-cheat-sheet)  
Une liste d'outils permettant d'utiliser git en mode graphique : [ici](https://git-scm.com/downloads/guis) et [là](https://git.wiki.kernel.org/index.php/InterfacesFrontendsAndTools#Graphical_Interfaces)  
[Flight rules for Git](https://github.com/k88hudson/git-flight-rules)

## Des Vidéos à ne pas manquer
   
Git and GitHub for Poets (une série de courtes vidéos par [Coding Train](https://twitter.com/thecodingtrain)) : [ici](https://www.youtube.com/playlist?list=PLRqwX-V7Uu6ZF9C0YMKuns9sLDzK6zoiV)  
Git++ : Passez au niveau supérieur de la gestion de version : [ici](https://www.youtube.com/watch?v=m0_C2cfM9IM) et [là](https://www.youtube.com/watch?v=rt-9mPaYtKo) avec les transparents [ici](http://webadeo.github.io/git-simpler-better-faster-stronger/#1.0)


## Des Références

Un guide pour personnaliser vos messages de commit avec des emojis est disponible [là](https://gitmoji.carloscuesta.me/)





